<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Password</name>
   <tag></tag>
   <elementGuidId>bec92d12-5181-4f61-a7c6-2d653b108770</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@name='Passwd']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>input[name=&quot;Passwd&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>a57e9ffd-cf0c-48a8-96b8-6ffea3004cde</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>password</value>
      <webElementGuid>72de2d9c-cb84-4d75-93a8-5624b028b03c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>whsOnd zHQkBf</value>
      <webElementGuid>b3ab99db-e054-48bd-a2aa-a101f0e7e930</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>jsname</name>
      <type>Main</type>
      <value>YPqjbf</value>
      <webElementGuid>58cc5a05-993d-4f9e-a519-b7f1c2e091af</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>current-password</value>
      <webElementGuid>83fc78d4-e756-4913-8e72-f2fa0b3f41f2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>spellcheck</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>ff6f722d-4d43-40fc-ad63-e6ed1ff94aac</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>0</value>
      <webElementGuid>5a873590-cb3f-4ab4-8065-92fb059eb610</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-label</name>
      <type>Main</type>
      <value>Enter your password</value>
      <webElementGuid>00954950-23e4-4dd6-8119-85688de3dee8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>Passwd</value>
      <webElementGuid>595628ce-dd9c-4303-8854-e52f67504f68</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocapitalize</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>10bd770e-3a1b-41b0-a50f-4c2a8659cfaf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>dir</name>
      <type>Main</type>
      <value>ltr</value>
      <webElementGuid>e2c7e416-2268-41e3-8141-eff5581c7c3c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-initial-dir</name>
      <type>Main</type>
      <value>ltr</value>
      <webElementGuid>f9e1cc4f-6249-4c04-a053-0a96608a6e6d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;password&quot;)/div[@class=&quot;aCsJod oJeWuf&quot;]/div[@class=&quot;aXBtI Wic03c&quot;]/div[@class=&quot;Xb9hP&quot;]/input[@class=&quot;whsOnd zHQkBf&quot;]</value>
      <webElementGuid>e30f6dfd-1287-46fa-b6c4-6af55c35d190</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@name='Passwd']</value>
      <webElementGuid>8345c9f3-5aac-4f60-95e0-2352c4565d87</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='password']/div/div/div/input</value>
      <webElementGuid>92a8fb16-bf2b-4154-b8f6-fc396d240cc2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div/div/div/input</value>
      <webElementGuid>772cdb48-e3bc-490d-8892-a764ab5d5d1d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'password' and @name = 'Passwd']</value>
      <webElementGuid>944a0354-3114-4e81-929d-e1fad45dff4b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
