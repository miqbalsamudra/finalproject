<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Profil</name>
   <tag></tag>
   <elementGuidId>6a805fee-ed5a-4f17-93c4-342bd1c51fed</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//aside[@id='sidebar-wrapper']/ul/li[4]/a</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>d6d0c0dc-0273-4c97-8d49-d23e69682da7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/dashboard/profile</value>
      <webElementGuid>70f05e3b-fa29-404b-b4a3-457aba43b045</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>nav-link</value>
      <webElementGuid>4cd7c1aa-8e9e-4a19-a372-b47f226699b9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Profil</value>
      <webElementGuid>58564a78-0d03-4194-91b1-e86e4050f4d1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;sidebar-wrapper&quot;)/ul[@class=&quot;sidebar-menu&quot;]/li[@class=&quot;dropdown&quot;]/a[@class=&quot;nav-link&quot;]</value>
      <webElementGuid>58375622-f79d-4976-b128-34ee11a7a85a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//aside[@id='sidebar-wrapper']/ul/li[4]/a</value>
      <webElementGuid>76e9f797-dd0e-4c2b-8453-2aced2cd3f12</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Basic Info'])[1]/following::a[1]</value>
      <webElementGuid>7b4fa1e8-8268-4310-a623-5475c79d4924</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Dashboard'])[1]/following::a[1]</value>
      <webElementGuid>3facb045-ebfe-4225-8ffd-e1ddf00b58e7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Billing Account'])[1]/preceding::a[1]</value>
      <webElementGuid>50483153-f846-4b5b-8f68-37bb1a2e5ca6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '/dashboard/profile')]</value>
      <webElementGuid>98d1c22f-b038-4805-a105-f91cd3da8be8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[4]/a</value>
      <webElementGuid>8d604805-00da-469c-98af-0ca2f8a36660</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/dashboard/profile' and (text() = 'Profil' or . = 'Profil')]</value>
      <webElementGuid>f061db49-b67b-4c6f-84e4-6952f16555d3</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
