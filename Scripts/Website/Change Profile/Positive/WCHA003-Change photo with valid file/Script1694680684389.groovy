import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.navigateToUrl(GlobalVariable.linkWebsiteDashboard)

WebUI.click(findTestObject('Website/Change Profile/a_Profil'))

WebUI.click(findTestObject('Website/Change Profile/a_Edit Profile'))

WebUI.delay(1)

WebUI.takeScreenshot('Screenshots/Website/Change Profile/WCHA003.png')

WebUI.uploadFile(findTestObject('Website/Change Profile/uploadFile'), CustomKeywords.'codingid.base.rootDirectory'() + '/Upload Files/Valid Photo.png')

WebUI.click(findTestObject('Website/Change Profile/button_Save Changes'))

WebUI.delay(5)

WebUI.click(findTestObject('Website/Change Profile/button_OK'))

WebUI.takeScreenshot('Screenshots/Website/Change Profile/WCHA003-2.png')

WebUI.delay(3)

