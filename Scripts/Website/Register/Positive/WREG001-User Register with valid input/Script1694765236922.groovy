import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.navigateToUrl(GlobalVariable.linkWebsite)

WebUI.click(findTestObject('Website/Register/button_BuatAkun'))

WebUI.setText(findTestObject('Website/Register/input_Nama_name'), nama)

WebUI.setText(findTestObject('Website/Register/input_Tanggal lahir_birth_date'), tgl_lahir)

WebUI.setText(findTestObject('Website/Register/input_E-Mail_email'), email)

WebUI.setText(findTestObject('Website/Register/input_Whatsapp_whatsapp'), whatsapp)

WebUI.setEncryptedText(findTestObject('Website/Register/input_Kata Sandi_password'), password)

WebUI.setEncryptedText(findTestObject('Website/Register/input_Konfirmasi kata sandi_password_confirmation'), konfirm_password)

WebUI.click(findTestObject('Website/Register/input_Konfirmasi kata sandi_inlineCheckbox1'))

WebUI.takeScreenshot('Screenshots/Website/Register/WREG001.png')

WebUI.click(findTestObject('Website/Register/button_Daftar'))

WebUI.verifyElementVisible(findTestObject('Website/Register/span_Verifikasi Email'))

WebUI.takeScreenshot('Screenshots/Website/Register/WREG001-2.png')

