import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.navigateToUrl(GlobalVariable.linkWebsite)

WebUI.click(findTestObject('Website/Login/LoginWithGoogleAccount/a_Masuk'))

WebUI.click(findTestObject('Website/Login/LoginWithGoogleAccount/button_Sign-in With Google'))

WebUI.setText(findTestObject('Website/Login/LoginWithGoogleAccount/input_Email'), GlobalVariable.notRegisteredEmail)

WebUI.takeScreenshot('Screenshots/Website/Login/WLOG011.png')

WebUI.click(findTestObject('Website/Login/LoginWithGoogleAccount/button_Next Email'))

WebUI.setEncryptedText(findTestObject('Website/Login/LoginWithGoogleAccount/input_Password'), GlobalVariable.validPassword)

WebUI.takeScreenshot('Screenshots/Website/Login/WLOG011-2.png')

WebUI.click(findTestObject('Website/Login/LoginWithGoogleAccount/button_Next Password'))

WebUI.verifyElementText(findTestObject('Website/Login/LoginWithGoogleAccount/verify_CreateNewAccount'), 'Create new account', 
    FailureHandling.STOP_ON_FAILURE)

WebUI.takeScreenshot('Screenshots/Website/Login/WLOG011-3.png')

