import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.navigateToUrl(GlobalVariable.linkWebsite)

WebUI.click(findTestObject('Website/Login/loginWithLoginButton/a_Masuk'))

WebUI.setEncryptedText(findTestObject('Website/Login/loginWithLoginButton/input_Kata Sandi_password'), GlobalVariable.validPassword)

WebUI.takeScreenshot('Screenshots/Website/Login/WLOG006.png')

WebUI.click(findTestObject('Website/Login/loginWithLoginButton/button_Login'))

WebUI.verifyEqual(WebUI.getAttribute(findTestObject('Website/Login/loginWithLoginButton/input_Email_email'), 'validationMessage'), 
    'Please fill out this field.')

WebUI.takeScreenshot('Screenshots/Website/Login/WLOG006-2.png')

