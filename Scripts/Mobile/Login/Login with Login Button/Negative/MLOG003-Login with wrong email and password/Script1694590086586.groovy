import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.tap(findTestObject('Mobile/HomePage/btnLoginHere'), 0)

Mobile.sendKeys(findTestObject('Mobile/LoginPage/inpEmail'), 'wrongemail@gmail.com')

Mobile.sendKeys(findTestObject('Mobile/LoginPage/inpPassword'), 'Wr@ngpass')

Mobile.takeScreenshot('Screenshots/Mobile/Login/MLOG003.png', FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Mobile/LoginPage/btnLogin'), 0)

Mobile.verifyElementExist(findTestObject('Mobile/LoginPage/txtInvalidCredential'), 0)

Mobile.takeScreenshot('Screenshots/Mobile/Login/MLOG003-2.png', FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Mobile/LoginPage/btnOk'), 0)

