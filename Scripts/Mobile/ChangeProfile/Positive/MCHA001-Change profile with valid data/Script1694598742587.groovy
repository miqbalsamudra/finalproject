import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.tap(findTestObject('Mobile/HomePage/btnProfile'), 0)

WebUI.delay(2)

Mobile.takeScreenshot('Screenshots/Mobile/Change Profile/MCHA001.png', FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Mobile/ProfilePage/iconGear'), 0)

WebUI.delay(2)

Mobile.takeScreenshot('Screenshots/Mobile/Change Profile/MCHA001-2.png', FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Mobile/ProfilePage/btnEditProfile'), 0)

WebUI.delay(2)

Mobile.takeScreenshot('Screenshots/Mobile/Change Profile/MCHA001-3.png', FailureHandling.STOP_ON_FAILURE)

Mobile.doubleTap(findTestObject('Mobile/EditProfilePage/editFullname'), 0)

Mobile.sendKeys(findTestObject('Mobile/EditProfilePage/editFullname'), 'team satu')

Mobile.doubleTap(findTestObject('Mobile/EditProfilePage/editPhone'), 0)

Mobile.sendKeys(findTestObject('Mobile/EditProfilePage/editPhone'), '081234567890')

Mobile.tap(findTestObject('Mobile/EditProfilePage/btnDate'), 0)

Mobile.tap(findTestObject('Mobile/EditProfilePage/btnDateOk'), 0)

WebUI.delay(2)

Mobile.takeScreenshot('Screenshots/Mobile/Change Profile/MCHA001-4.png', FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Mobile/EditProfilePage/btnSaveChanges'), 0)

WebUI.delay(2)

Mobile.takeScreenshot('Screenshots/Mobile/Change Profile/MCHA001-5.png', FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Mobile/EditProfilePage/btnOkay'), 0)

WebUI.delay(2)

Mobile.takeScreenshot('Screenshots/Mobile/Change Profile/MCHA001-6.png', FailureHandling.STOP_ON_FAILURE)

