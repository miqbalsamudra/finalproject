import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject

import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.webui.keyword.internal.WebUIAbstractKeyword
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile

import internal.GlobalVariable as GlobalVariable

import com.kms.katalon.core.annotation.BeforeTestCase
import com.kms.katalon.core.annotation.BeforeTestSuite
import com.kms.katalon.core.annotation.AfterTestCase
import com.kms.katalon.core.annotation.AfterTestSuite
import com.kms.katalon.core.context.TestCaseContext
import com.kms.katalon.core.context.TestSuiteContext

class CodingIDTestListener {
	/**
	 * Executes before every test case starts.
	 * @param testCaseContext related information of the executed test case.
	 */
	@BeforeTestCase
	def BeforeTestCase(TestCaseContext testCaseContext) {
		
		String testCaseId = testCaseContext.getTestCaseId()
		
		if (testCaseId.contains("Website")) {
			
			WebUI.openBrowser('')
			WebUI.maximizeWindow()
			
			if (testCaseId.contains("Change Profile") || testCaseId.contains("Access")) {
				
				WebUI.navigateToUrl(GlobalVariable.linkWebsite)
				
				WebUI.click(findTestObject('Website/Login/loginWithLoginButton/a_Masuk'))
				
				WebUI.setText(findTestObject('Website/Login/loginWithLoginButton/input_Email_email'), GlobalVariable.validEmail)
				
				WebUI.setEncryptedText(findTestObject('Website/Login/loginWithLoginButton/input_Kata Sandi_password'),
					GlobalVariable.validPassword)
				
				WebUI.click(findTestObject('Website/Login/loginWithLoginButton/button_Login'))
				
			}
			
		} else if (testCaseId.contains("Mobile")) {
			
			if (testCaseId.contains("Access profile") || testCaseId.contains("ChangeProfile")) {
				
				Mobile.startApplication(CustomKeywords.'codingid.base.rootDirectory'() + '/androidapp/DemoAppV2.apk', true)
		
				Mobile.tap(findTestObject('Mobile/HomePage/btnLoginHere'), 0)
		
				Mobile.sendKeys(findTestObject('Mobile/LoginPage/inpEmail'), 'team1batch10@gmail.com')
		
				Mobile.sendKeys(findTestObject('Mobile/LoginPage/inpPassword'), '@Password1')
		
				Mobile.tap(findTestObject('Mobile/LoginPage/btnLogin'), 0)
		
				Mobile.verifyElementExist(findTestObject('Mobile/HomePage/iconCart'), 0)
				
				Mobile.delay(2)
				
			} else if (testCaseId.contains("Login")) {
				
				Mobile.startApplication(CustomKeywords.'codingid.base.rootDirectory'() + '/androidapp/DemoAppV2.apk', true)
				
				Mobile.delay(2)
			}
			
		} else if (testCaseId.contains("API")) {
			
		}
	}

	/**
	 * Executes after every test case ends.
	 * @param testCaseContext related information of the executed test case.
	 */
	@AfterTestCase
	def sampleAfterTestCase(TestCaseContext testCaseContext) {
		
		String testCaseId = testCaseContext.getTestCaseId()
		
		if (testCaseId.contains("Website")) {
			
			WebUI.closeBrowser()
			
		} else if (testCaseId.contains("Mobile")) {
			
			Mobile.closeApplication()	
			
		} else if (testCaseId.contains("API")) {
			
		}
	}
}