<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Login Mobile Test Suites</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>c82ff125-d482-48b6-bd58-8840085b9958</testSuiteGuid>
   <testCaseLink>
      <guid>b35f83c2-b204-4828-82a8-10e61bf087a7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Mobile/Login/Login with Login Button/Positive/MLOG001-Login with valid email and password</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>5c29613b-6645-415f-8bf0-2d8ae67f5eb1</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>7c7a2db2-7014-457f-a005-046146ecba77</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>145e45f3-c21a-4ba2-bceb-f902ef727290</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Mobile/Login/Login with Login Button/Positive/MLOG002-Access profile with valid login</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>dba9059a-a922-45a8-a9ba-b3832224eafa</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Mobile/Login/Login with Login Button/Negative/MLOG003-Login with wrong email and password</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>48ef6d72-4086-496d-98c3-b98d205e29fd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Mobile/Login/Login with Login Button/Negative/MLOG004-Login with wrong email</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>ebbcf15b-a75f-42fc-9a11-38c3a9afaa4f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Mobile/Login/Login with Login Button/Negative/MLOG005-Login with wrong password</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>923f6be9-7e2c-43f4-8f32-78e02c3d4929</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Mobile/Login/Login with Login Button/Negative/MLOG006-Login with invalid email and wrong password</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>3dd2d3a2-050e-4799-95e6-005d237b5b8a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Mobile/Login/Login with Login Button/Negative/MLOG007-Login with invalid email</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>9255be2b-dc9f-48b1-90f9-b558a3c762f4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Mobile/Login/Login with Login Button/Negative/MLOG008-Login with blank email</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>fda283dd-d072-486b-9cf3-5ef94eed25eb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Mobile/Login/Login with Login Button/Negative/MLOG009-Login with blank password</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>63eec84a-9224-4529-b79f-23c9ef3e42db</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Mobile/Login/Login with Login Button/Negative/MLOG010-Login with blank email and blank password</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>16a88e9c-39d4-4c6d-8f97-cf4ca5188d8e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Mobile/Login/Login with Login Button/Negative/MLOG011-Login with invalid email and blank password</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>24bd9d50-fe97-4be7-8edd-5acd9e38d690</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Mobile/Login/Login with Google Account/Positive/MLOG012-Login with registered Google Account</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>488492c4-e147-440a-9509-d3f7e25d326d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Mobile/Login/Login with Google Account/Negative/MLOG013-Login without registered Google Account</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
</TestSuiteEntity>
